import React from 'react'
import {Card, Button} from 'antd'

import './ui.less'

export default class Buttons extends React.Component {
    render() {
        return (
            <div>
                <Card title={"基础按钮"} className={"card-wrap"}>
                    <Button type="primary">Primary</Button>
                    <Button>Default</Button>
                    <Button type="dashed">Dashed</Button>
                    <Button type="danger">Danger</Button>
                </Card>
            </div>
        );
    }
}
